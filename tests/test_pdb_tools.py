import os.path as op
import pandas as pd
from tempfile import TemporaryDirectory
from common import pdb_tools


# %%
class TestPresent:

    def test_1(self):
        sifts_data = pdb_tools.get_sifts_data('1arr', op.splitext(__file__)[0])
        assert isinstance(sifts_data, pd.DataFrame)
        assert not sifts_data.empty

    def test_2(self):
        sifts_data = pdb_tools.get_sifts_data('3mbp', op.splitext(__file__)[0])
        assert isinstance(sifts_data, pd.DataFrame)
        assert not sifts_data.empty


class TestAbscent:

    @classmethod
    def setup_class(cls):
        cls.temp_dir = TemporaryDirectory()

    @classmethod
    def teardown_class(cls):
        cls.temp_dir.cleanup()

    def test_1(self):
        sifts_data = pdb_tools.get_sifts_data('1arr', self.temp_dir)
        assert isinstance(sifts_data, pd.DataFrame)
        assert not sifts_data.empty

    def test_2(self):
        sifts_data = pdb_tools.get_sifts_data('3mbp', self.temp_dir)
        assert isinstance(sifts_data, pd.DataFrame)
        assert not sifts_data.empty
        del sifts_data


# %%
if __name__ == '__main__':
    import pytest
    pytest.main([__file__, '-svx'])
