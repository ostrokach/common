import pycurl


# %%
def download(url, output_file):
    """Download file from 'url' into 'output_file'.
    """
    with open(output_file, 'wb') as ofh:
        c = pycurl.Curl()
        c.setopt(c.URL, url)
        c.setopt(c.WRITEDATA, ofh)
        c.perform()
        c.close()
