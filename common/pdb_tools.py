import os
import os.path as op
import gzip
import logging
from functools import lru_cache
from lxml import etree

import pandas as pd

from elaspic.structure_tools import AAA_DICT

from .system_tools import download


# %%
logger = logging.getLogger(__name__)


# %% SIFTS
@lru_cache(maxsize=512)
def get_sifts_data(pdb_id, cache_dir, cache_dict={}):
    """
    Download the xml file for the pdb file with the pdb id pdb_id, parse that
    xml file, and return a dictionry which maps pdb resnumbing to uniprot
    numbering for the chain specified by pdb_chain and uniprot specified by
    uniprot_id.
    """
    if pdb_id in cache_dict:
        return cache_dict[pdb_id]

    # Download the sifts file if it is not in cache
    sifts_filename = pdb_id.lower() + '.xml.gz'
    if not os.path.isfile(op.join(cache_dir, sifts_filename)):
        url = 'ftp://ftp.ebi.ac.uk/pub/databases/msd/sifts/xml/' + sifts_filename
        download(url, op.join(cache_dir, sifts_filename))

    # Go over the xml file and find all cross-references to uniprot
    pdb_sifts_data = []
    with gzip.open(op.join(cache_dir, sifts_filename)) as ifh:
        root = etree.fromstring(ifh.read())
    for entity in root:
        # Go over different entries in SIFTS
        if entity.tag.split('}')[-1] == 'entity':
                # Go over different chain segments
                for segment in entity:
                    if segment.tag.split('}')[-1] == 'segment':
                        # Go over different lists of residues
                        for listResidue in segment:
                            if listResidue.tag.split('}')[-1] == 'listResidue':
                                # Go over each residue
                                for residue in listResidue:
                                    if residue.tag.split('}')[-1] == 'residue':
                                        # Go over all database crossreferences keeping only those
                                        # that come from uniprot and match the given uniprot_id
                                        residue_data = _fill_residue_data(pdb_id, residue)
                                        if residue_data is None:
                                            continue
                                        pdb_sifts_data.append(residue_data)

    # Convert data to a DataFrame and make sure we have no duplicates
    pdb_sifts_data_df = pd.DataFrame(pdb_sifts_data)
    assert len(pdb_sifts_data_df) == len(
        pdb_sifts_data_df.drop_duplicates(subset=['pdb_chain', 'resnum']))

    # TODO: should optimise the code above instead of simply removing duplicates
    # pdb_sifts_data_df = pdb_sifts_data_df.drop_duplicates()

    cache_dict[pdb_id] = pdb_sifts_data_df
    return pdb_sifts_data_df


def _fill_residue_data(pdb_id, residue_xml):
    residue_data = {'is_observed': True, 'comments': []}

    for crossRefDb in residue_xml:
        # Some more details about the residue
        if crossRefDb.tag.split('}')[-1] == 'residueDetail':
            residue_data['comments'].append(crossRefDb.text)
            if crossRefDb.text == 'Not_Observed':
                residue_data['is_observed'] = False
        # Mappings to other databases
        if crossRefDb.tag.split('}')[-1] == 'crossRefDb':
            if (crossRefDb.attrib.get('dbSource') == 'PDB'):
                    residue_data['pdb_id'] = crossRefDb.attrib.get('dbAccessionId')
                    residue_data['pdb_chain'] = crossRefDb.attrib.get('dbChainId')
                    residue_data['resnum'] = crossRefDb.attrib.get('dbResNum')
                    resname = crossRefDb.attrib.get('dbResName')
                    if resname in AAA_DICT:
                        residue_data['pdb_aa'] = AAA_DICT[resname]
                    else:
                        logger.warning('Could not convert amino acid {} to a one letter code!')
                        residue_data['pdb_aa'] = resname
            if (crossRefDb.attrib.get('dbSource') == 'UniProt'):
                    residue_data['uniprot_id'] = crossRefDb.attrib.get('dbAccessionId')
                    residue_data['uniprot_position'] = crossRefDb.attrib.get('dbResNum')
                    residue_data['uniprot_aa'] = crossRefDb.attrib.get('dbResName')
            if (crossRefDb.attrib.get('dbSource') == 'Pfam'):
                    residue_data['pfam_id'] = crossRefDb.attrib.get('dbAccessionId')

    residue_data['comments'] = ','.join(residue_data['comments'])

    return residue_data
